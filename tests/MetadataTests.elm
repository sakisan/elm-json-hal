module MetadataTests exposing (suite)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Metadata exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "module Metadata" <|
        [ describe "keyToString" <|
            List.map
                (\( key, getKeyString ) ->
                    test getKeyString <|
                        \_ ->
                            Expect.equal (keyToString key) getKeyString
                )
                [ ( dcTitle, "dc.title" )
                , ( dcContributorAuthor, "dc.contributor.author" )
                , ( dcDescription, "dc.description" )
                ]
        , describe "keyFromString" <|
            List.map
                (\( key, getKeyString ) ->
                    test getKeyString <|
                        \_ ->
                            Expect.equal (keyFromString getKeyString) key
                )
                [ ( Just dcTitle, "dc.title" )
                , ( Just dcContributorAuthor, "dc.contributor.author" )
                , ( Just dcDescription, "dc.description" )
                , ( Nothing, "dc" )
                , ( Nothing, "dc.." )
                ]
        ]
