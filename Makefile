index.html: *.elm
	TZ=UTC-2 date +"%Y-%m-%d %H:%M:%S" > compiler.elmc
	time elm-make Main.elm --yes --warn --debug >> compiler.elmc 2>&1
