module Metadata exposing
    ( Element
    , Language
    , MetadataKey
    , MetadataValue
    , Qualifier
    , Schema
    , dcContributorAuthor
    , dcDescription
    , dcTitle
    , getKey
    , keyFromString
    , getKeyString
    , keyToString
    , languageFromString
    )

import NonEmptyString exposing (NonEmptyString, fromString_)


type alias MetadataKey =
    ( Schema, Element, Qualifier )


type Schema
    = Schema NonEmptyString


type Element
    = Element NonEmptyString


type Qualifier
    = NoQualifier
    | Qualifier NonEmptyString


type Language
    = EN
    | EN_US


type alias MetadataValue =
    { schema : Schema
    , element : Element
    , qualifier : Qualifier
    , language : Maybe Language
    , value : String
    }


schemaString : Schema -> String
schemaString (Schema string) =
    NonEmptyString.toString string


elementString : Element -> String
elementString (Element string) =
    NonEmptyString.toString string


qualifierString : Qualifier -> Maybe String
qualifierString qualifier =
    case qualifier of
        NoQualifier ->
            Nothing

        Qualifier string ->
            Just <| NonEmptyString.toString string


getKey : MetadataValue -> MetadataKey
getKey { schema, element, qualifier } =
    ( schema, element, qualifier )


getKeyString : MetadataValue -> String
getKeyString =
    getKey >> keyToString


keyToString : MetadataKey -> String
keyToString ( schema, element, qualifier ) =
    [ Just (schemaString schema)
    , Just (elementString element)
    , qualifierString qualifier
    ]
        |> List.filterMap identity
        |> String.join "."


keyFromString : String -> Maybe MetadataKey
keyFromString key =
    case String.split "." key of
        schema :: element :: maybeMore ->
            Maybe.map3
                (\a b c -> ( a, b, c ))
                (NonEmptyString.to Schema schema)
                (NonEmptyString.to Element element)
                (case List.head maybeMore of
                    Nothing ->
                        Just NoQualifier

                    Just string ->
                        NonEmptyString.to Qualifier string
                )

        _ ->
            Nothing


keyFromTuple : ( NonEmptyString, NonEmptyString, String ) -> MetadataKey
keyFromTuple ( schema, element, qualifier ) =
    ( Schema schema
    , Element element
    , NonEmptyString.to Qualifier qualifier
        |> Maybe.withDefault NoQualifier
    )


keyFromString_ : String -> MetadataKey
keyFromString_ key =
    let
        defaultKey =
            keyFromTuple
                ( fromString_ "default_schema"
                , fromString_ "default_element"
                , ""
                )
    in
    keyFromString key
        |> Maybe.withDefault defaultKey


languageFromString : String -> Maybe Language
languageFromString language =
    case String.toLower language of
        "en" ->
            Just EN

        "en_us" ->
            Just EN_US

        _ ->
            Nothing


dcTitle : MetadataKey
dcTitle =
    keyFromString_ "dc.title"


dcContributorAuthor : MetadataKey
dcContributorAuthor =
    keyFromString_ "dc.contributor.author"


dcDescription : MetadataKey
dcDescription =
    keyFromString_ "dc.description"
