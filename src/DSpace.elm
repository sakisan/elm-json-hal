module DSpace exposing
    ( Collection
    , CollectionID
    , Community
    , CommunityID
    , DSpaceObject(..)
    , Handle
    , ID
    , Site
    , collectionID
    , communityID
    , filterCollections
    , filterCommunities
    , getIDString
    , getName
    , handle
    , handleToString
    , idToString
    )

import Metadata exposing (MetadataValue)


type ID a
    = ID String


type Handle
    = Handle String String


type Site
    = Site


type CommunityType
    = CommunityType


type alias CommunityID =
    ID CommunityType


type alias Community =
    { uuid : CommunityID
    , name : String
    , handle : Handle
    , metadata : List MetadataValue
    , collections : List CollectionID
    , collectionsLink : String
    , logoLink : Maybe String
    , selfLink : String
    }


type CollectionType
    = CollectionType


type alias CollectionID =
    ID CollectionType


type alias Collection =
    { uuid : CollectionID
    , name : String
    , handle : Handle
    , metadata : List MetadataValue
    , defaultAccesConditionsLink : Maybe String
    , licenseLink : Maybe String
    , logoLink : Maybe String
    , selfLink : String
    }


type DSpaceObject
    = CommunityDso Community
    | CollectionDso Collection


id : a -> String -> ID a
id phantom uuid =
    ID uuid


collectionID : String -> ID CollectionType
collectionID =
    id CollectionType


communityID : String -> ID CommunityType
communityID =
    id CommunityType


idToString : ID a -> String
idToString (ID string) =
    string


getIDString : DSpaceObject -> String
getIDString dso =
    case dso of
        CommunityDso community ->
            idToString community.uuid

        CollectionDso collection ->
            idToString collection.uuid


handle : String -> Maybe Handle
handle input =
    case String.split "/" input of
        prefix :: suffix :: [] ->
            Just <| Handle prefix suffix

        _ ->
            Nothing


handleToString : Handle -> String
handleToString (Handle prefix suffix) =
    prefix ++ "/" ++ suffix


filterCommunities : List DSpaceObject -> List Community
filterCommunities list =
    let
        helper : DSpaceObject -> Maybe Community
        helper dso =
            case dso of
                CommunityDso community ->
                    Just community

                _ ->
                    Nothing
    in
    List.filterMap helper list


filterCollections : List DSpaceObject -> List Collection
filterCollections list =
    let
        helper : DSpaceObject -> Maybe Collection
        helper dso =
            case dso of
                CollectionDso collection ->
                    Just collection

                _ ->
                    Nothing
    in
    List.filterMap helper list


getName : DSpaceObject -> String
getName dso =
    case dso of
        CommunityDso community ->
            community.name

        CollectionDso collection ->
            collection.name
