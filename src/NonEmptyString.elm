module NonEmptyString exposing
    ( NonEmptyString
    , fromString
    , fromString_
    , singleChar
    , to
    , toString
    )


type NonEmptyString
    = NonEmptyString Char String


toString : NonEmptyString -> String
toString (NonEmptyString char string) =
    String.fromChar char ++ string


fromString : String -> Maybe NonEmptyString
fromString string =
    case String.toList string of
        [] ->
            Nothing

        x :: xs ->
            Just <| NonEmptyString x (String.fromList xs)


fromString_ : String -> NonEmptyString
fromString_ input =
    Maybe.withDefault (singleChar '_') (fromString input)


to : (NonEmptyString -> a) -> String -> Maybe a
to tag string =
    Maybe.map tag (fromString string)


singleChar : Char -> NonEmptyString
singleChar char =
    NonEmptyString char ""
