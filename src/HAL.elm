module HAL exposing
    ( HalRecord
    , Link
    , Links
    , halDecoder
    , hrefDecoder
    )

import Dict exposing (Dict)
import Json.Decode as Decode



{-
   type alias HalData main embedded =
           WebData (HalRecord main embedded)
-}


type alias HalRecord a b =
    { object : a, embedded : b, links : Links }


type alias Link =
    { href : String }


type alias Links =
    Dict String Link


halDecoder : (b -> Links -> Decode.Decoder a) -> Decode.Decoder b -> Decode.Decoder (HalRecord a b)
halDecoder mainDecoder embeddedDecoder =
    let
        recordDecoder : b -> Links -> Decode.Decoder (HalRecord a b)
        recordDecoder embedded links =
            mainDecoder embedded links
                |> Decode.map (\main -> HalRecord main embedded links)
    in
    Decode.map2 recordDecoder
        (Decode.field "_embedded" embeddedDecoder)
        (Decode.field "_links" (Decode.dict linkDecoder))
        |> Decode.andThen identity



{- "_links": {
     "self": { "href": "/blog-post" },
     "author": { "href": "/people/alan-watts" }
   }
-}


linkDecoder : Decode.Decoder Link
linkDecoder =
    Decode.map Link
        (Decode.field "href" Decode.string)


hrefDecoder : String -> Links -> Decode.Decoder String
hrefDecoder linkName links =
    case Dict.get linkName links of
        Nothing ->
            Decode.fail <| linkName ++ " is missing from the links"

        Just link ->
            Decode.succeed link.href
