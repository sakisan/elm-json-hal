module Repository exposing
    ( Repository
    , empty
    , getChildren
    , getCommunities
    , getDSpaceObject
    , insert
    , insertList
    , loading
    , successes
    )

import DSpace exposing (..)
import Dict exposing (Dict)
import RemoteData exposing (WebData)


type Repository
    = Repository (Dict String (WebData DSpaceObject))


empty : Repository
empty =
    Repository Dict.empty


insert : ID a -> WebData DSpaceObject -> Repository -> Repository
insert id webData (Repository dict) =
    Dict.insert (idToString id) webData dict
        |> Repository


insertList : List DSpaceObject -> Repository -> Repository
insertList list (Repository dict) =
    let
        helper : DSpaceObject -> Dict String (WebData DSpaceObject) -> Dict String (WebData DSpaceObject)
        helper dso newDict =
            Dict.insert
                (getIDString dso)
                (RemoteData.Success dso)
                newDict
    in
    List.foldl helper dict list
        |> Repository


getCommunities : Repository -> List Community
getCommunities (Repository dict) =
    Dict.values dict
        |> successes
        |> filterCommunities


successes : List (RemoteData.RemoteData e a) -> List a
successes list =
    let
        helper remote result =
            case remote of
                RemoteData.Success member ->
                    member :: result

                _ ->
                    result
    in
    List.foldl helper [] list


getDSpaceObject : Repository -> ID a -> WebData DSpaceObject
getDSpaceObject (Repository dict) id =
    case Dict.get (idToString id) dict of
        Nothing ->
            RemoteData.NotAsked

        Just data ->
            data


loading : ID a -> Repository -> Repository
loading id (Repository dict) =
    Dict.insert (idToString id) RemoteData.Loading dict
        |> Repository


getChildren : Repository -> DSpaceObject -> List (WebData DSpaceObject)
getChildren ((Repository dict) as repository) dso =
    case dso of
        CommunityDso community ->
            community.collections
                |> List.map (getDSpaceObject repository)

        CollectionDso collection ->
            []
