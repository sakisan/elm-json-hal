module FetchData exposing (FetchData(..), Pagination, api, apiPath, decodeCollection, decodeCollections, decodeCommunities, decodeCommunity, decodeEmbeddedCollections, decodePage, fetchCollection, fetchCommunity, fetchCommunityList, handleDecoder, metadataDecoder)

import DSpace
    exposing
        ( Collection
        , CollectionID
        , Community
        , CommunityID
        , DSpaceObject(..)
        , Handle
        , ID
        )
import Dict
import HAL exposing (HalRecord, Links)
import Http
import Json.Decode as Decode
import Metadata exposing (Element, Language, MetadataValue, Qualifier, Schema)
import RemoteData exposing (WebData)


api : String
api =
    "https://dspace7.4science.it/dspace-spring-rest/api"


apiPath : String -> String
apiPath path =
    api ++ path


type alias Pagination =
    { size : Int
    , totalElements : Int
    , totalPages : Int
    , number : Int
    }


type FetchData
    = FetchedCommunityList (WebData (HalRecord Pagination (List DSpaceObject)))
    | FetchedCommunity ( CommunityID, WebData (HalRecord Community (List Collection)) )
    | FetchedCollection ( CollectionID, WebData (HalRecord Collection ()) )


andMap : Decode.Decoder a -> Decode.Decoder (a -> b) -> Decode.Decoder b
andMap =
    Decode.map2 (|>)


decodePage : Decode.Decoder Pagination
decodePage =
    Decode.field "page"
        (Decode.succeed Pagination
            |> andMap (Decode.field "size" Decode.int)
            |> andMap (Decode.field "totalElements" Decode.int)
            |> andMap (Decode.field "totalPages" Decode.int)
            |> andMap (Decode.field "number" Decode.int)
        )


handleDecoder : Decode.Decoder Handle
handleDecoder =
    Decode.string
        |> Decode.andThen
            (\string ->
                case DSpace.handle string of
                    Just aHandle ->
                        Decode.succeed aHandle

                    Nothing ->
                        Decode.fail (string ++ " is not a handle")
            )


metadataDecoder : Decode.Decoder (List MetadataValue)
metadataDecoder =
    let
        validate : String -> Maybe a -> Decode.Decoder a
        validate errorMessage maybe =
            case maybe of
                Just value ->
                    Decode.succeed value

                Nothing ->
                    Decode.fail errorMessage

        decodeKey : Decode.Decoder ( Schema, Element, Qualifier )
        decodeKey =
            Decode.string
                |> Decode.map Metadata.keyFromString
                |> Decode.andThen (validate "invalid key")

        decodeLanguage : Decode.Decoder (Maybe Language)
        decodeLanguage =
            Decode.string
                |> Decode.map Metadata.languageFromString
                |> Decode.andThen (validate "invalid language")
                |> Decode.nullable

        constructMetadataValue : ( Schema, Element, Qualifier ) -> Maybe Language -> String -> MetadataValue
        constructMetadataValue ( schema, element, qualifier ) language value =
            MetadataValue schema element qualifier language value

        decodeMetadataValue : Decode.Decoder MetadataValue
        decodeMetadataValue =
            Decode.succeed constructMetadataValue
                |> andMap (Decode.field "key" decodeKey)
                |> andMap (Decode.field "language" decodeLanguage)
                |> andMap (Decode.field "value" Decode.string)
    in
    Decode.list decodeMetadataValue


decodeCommunity : List Collection -> Links -> Decode.Decoder Community
decodeCommunity collections links =
    Decode.succeed Community
        |> andMap (Decode.field "uuid" (Decode.map DSpace.communityID Decode.string))
        |> andMap (Decode.field "name" Decode.string)
        |> andMap (Decode.field "handle" handleDecoder)
        |> andMap (Decode.field "metadata" metadataDecoder)
        |> andMap (Decode.succeed (List.map .uuid collections))
        |> andMap (HAL.hrefDecoder "collections" links)
        |> andMap (Decode.succeed (Dict.get "logo" links |> Maybe.map .href))
        |> andMap (HAL.hrefDecoder "self" links)


decodeCommunities : Decode.Decoder (List DSpaceObject)
decodeCommunities =
    HAL.halDecoder decodeCommunity decodeEmbeddedCollections
        |> Decode.map
            (\hal ->
                CommunityDso hal.object :: List.map CollectionDso hal.embedded
            )
        |> Decode.list
        |> Decode.map List.concat
        |> Decode.field "communities"


decodeCollection : () -> Links -> Decode.Decoder Collection
decodeCollection _ links =
    Decode.succeed Collection
        |> andMap (Decode.field "uuid" (Decode.map DSpace.collectionID Decode.string))
        |> andMap (Decode.field "name" Decode.string)
        |> andMap (Decode.field "handle" handleDecoder)
        |> andMap (Decode.field "metadata" metadataDecoder)
        |> andMap (Decode.succeed (Dict.get "defaultAccessConditions" links |> Maybe.map .href))
        |> andMap (Decode.succeed (Dict.get "license" links |> Maybe.map .href))
        |> andMap (Decode.succeed (Dict.get "logo" links |> Maybe.map .href))
        |> andMap (HAL.hrefDecoder "self" links)


decodeEmbeddedCollections : Decode.Decoder (List Collection)
decodeEmbeddedCollections =
    HAL.halDecoder (\collections links -> decodePage) decodeCollections
        |> Decode.map .embedded
        |> Decode.field "collections"


decodeCollections : Decode.Decoder (List Collection)
decodeCollections =
    HAL.halDecoder decodeCollection (Decode.succeed ())
        |> Decode.map .object
        |> Decode.list
        |> Decode.field "collections"


fetchCommunity : CommunityID -> (FetchData -> msg) -> Cmd msg
fetchCommunity id constructor =
    HAL.halDecoder decodeCommunity decodeEmbeddedCollections
        |> Http.get (apiPath "/core/communities/" ++ DSpace.idToString id)
        |> RemoteData.sendRequest
        |> Cmd.map (\data -> constructor <| FetchedCommunity ( id, data ))


fetchCommunityList : (FetchData -> msg) -> Cmd msg
fetchCommunityList constructor =
    HAL.halDecoder (\_ links -> decodePage) decodeCommunities
        |> Http.get (apiPath "/core/communities")
        |> RemoteData.sendRequest
        |> Cmd.map (constructor << FetchedCommunityList)


fetchCollection : CollectionID -> (FetchData -> msg) -> Cmd msg
fetchCollection id constructor =
    HAL.halDecoder decodeCollection (Decode.succeed ())
        |> Http.get (apiPath "/core/collections/" ++ DSpace.idToString id)
        |> RemoteData.sendRequest
        |> Cmd.map (\data -> constructor <| FetchedCollection ( id, data ))
