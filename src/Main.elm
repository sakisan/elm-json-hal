module Main exposing (main)

import Browser
import Browser.Dom as Dom
import Browser.Navigation as Nav
import DSpace
    exposing
        ( Collection
        , CollectionID
        , Community
        , CommunityID
        , DSpaceObject(..)
        , Handle
        , ID
        )
import Dict exposing (Dict)
import Element exposing (Element)
import Element.Background as Background
import Element.Font as Font
import Element.Region as Region
import FetchData exposing (..)
import HAL exposing (HalRecord, Link, Links)
import Html
import Http
import Metadata exposing (MetadataValue)
import RemoteData exposing (WebData)
import Repository exposing (Repository)
import Task
import Url
import Url.Parser as UrlParser exposing ((</>))


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL


type Page
    = Home
    | CommunityPage CommunityID
    | CollectionPage CollectionID


type alias SearchData =
    WebData
        { pagination : Pagination
        , search : Maybe String
        }


type alias Model =
    { currentPage : Page
    , repository : Repository
    , homePage : SearchData
    , navKey : Nav.Key
    }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    showPage
        { currentPage = Home
        , repository = Repository.empty
        , homePage = RemoteData.NotAsked
        , navKey = key
        }
        Home


withCurrentPage : Page -> Model -> Model
withCurrentPage page model =
    { model | currentPage = page }


updateRepository : (Repository -> Repository) -> Model -> Model
updateRepository updater ({ repository } as model) =
    { model | repository = updater repository }



-- UPDATE


type Msg
    = Fetched FetchData
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | ShowPage Page


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        _ =
            log "msg" ( msg, model )
    in
    case msg of
        Fetched webdata ->
            ( fetched webdata model, Cmd.none )

        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.navKey (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            showPage model (urlToPage url)

        ShowPage page ->
            showPage model page


showPage : Model -> Page -> ( Model, Cmd Msg )
showPage model page =
    let
        fetchDsoIfNecessary fetch id =
            Repository.getDSpaceObject model.repository id
                |> ifNotAsked
                    ( updateRepository (Repository.loading id) model
                    , fetch id Fetched
                    )
                |> orElse ( model, Cmd.none )

        ( newModel, command ) =
            case page of
                Home ->
                    model.homePage
                        |> ifNotAsked
                            ( { model | homePage = RemoteData.Loading }
                            , fetchCommunityList Fetched
                            )
                        |> orElse ( model, Cmd.none )

                CommunityPage id ->
                    fetchDsoIfNecessary fetchCommunity id

                CollectionPage id ->
                    fetchDsoIfNecessary fetchCollection id
    in
    ( withCurrentPage page newModel, command )


urlToPage : Url.Url -> Page
urlToPage url =
    let
        idParser : (String -> ID b) -> UrlParser.Parser (ID b -> a) a
        idParser constructor =
            UrlParser.map constructor UrlParser.string

        pageParser : UrlParser.Parser (Page -> a) a
        pageParser =
            UrlParser.oneOf
                [ UrlParser.map Home UrlParser.top
                , UrlParser.map CommunityPage
                    (UrlParser.s "communities" </> idParser DSpace.communityID)
                , UrlParser.map CollectionPage
                    (UrlParser.s "collections" </> idParser DSpace.collectionID)
                ]
    in
    UrlParser.parse pageParser url
        |> Maybe.withDefault Home


toLocation : DSpaceObject -> String
toLocation dso =
    case dso of
        CommunityDso community ->
            "/communities/" ++ DSpace.idToString community.uuid

        CollectionDso collection ->
            "/collections/" ++ DSpace.idToString collection.uuid


fetched : FetchData -> Model -> Model
fetched fetchData model =
    let
        logIfFailure : WebData a -> WebData a
        logIfFailure webData =
            case webData of
                RemoteData.Failure e ->
                    Debug.log "fetched failure" e
                        |> RemoteData.Failure

                _ ->
                    webData
    in
    case fetchData of
        FetchedCommunityList data ->
            fetchedCommunities (logIfFailure data) model

        FetchedCommunity ( id, data ) ->
            fetchedCommunity ( id, logIfFailure data ) model

        FetchedCollection ( id, data ) ->
            fetchedCollection ( id, logIfFailure data ) model


fetchedCommunity : ( CommunityID, HalData Community (List Collection) ) -> Model -> Model
fetchedCommunity ( id, remoteHalCommunity ) model =
    let
        remoteCommunity : WebData DSpaceObject
        remoteCommunity =
            RemoteData.map (.object >> CommunityDso) remoteHalCommunity

        collections : List DSpaceObject
        collections =
            remoteHalCommunity
                |> RemoteData.map (.embedded >> List.map CollectionDso)
                |> RemoteData.withDefault []
    in
    { model
        | repository =
            model.repository
                |> Repository.insert id remoteCommunity
                |> Repository.insertList collections
    }


fetchedCollection : ( CollectionID, HalData Collection () ) -> Model -> Model
fetchedCollection ( id, remoteHalCollection ) model =
    let
        remoteCollection : WebData DSpaceObject
        remoteCollection =
            RemoteData.map (.object >> CollectionDso) remoteHalCollection
    in
    { model
        | repository =
            model.repository
                |> Repository.insert id remoteCollection
    }


fetchedCommunities : HalData Pagination (List DSpaceObject) -> Model -> Model
fetchedCommunities remoteHal model =
    let
        updateModel : HalRecord Pagination (List DSpaceObject) -> Model
        updateModel hal =
            { model
                | repository = Repository.insertList hal.embedded model.repository
                , homePage =
                    RemoteData.Success
                        { pagination = hal.object
                        , search = Dict.get "search" hal.links |> Maybe.map .href
                        }
            }
    in
    case remoteHal of
        RemoteData.Success data ->
            updateModel data

        RemoteData.NotAsked ->
            { model | homePage = RemoteData.NotAsked }

        RemoteData.Loading ->
            { model | homePage = RemoteData.Loading }

        RemoteData.Failure e ->
            { model | homePage = RemoteData.Failure e }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- UTILS


log =
    Debug.log


type alias HalData main embedded =
    WebData (HalRecord main embedded)


ifNotAsked : a -> RemoteData.RemoteData b c -> Maybe a
ifNotAsked value remote =
    case remote of
        RemoteData.NotAsked ->
            Just value

        RemoteData.Loading ->
            Nothing

        RemoteData.Failure _ ->
            Nothing

        RemoteData.Success _ ->
            Nothing


orElse : a -> Maybe a -> a
orElse =
    Maybe.withDefault



-- STYLE


white =
    Element.rgb 1 1 1


grey =
    Element.rgb255 238 238 238


blue =
    Element.rgb255 0x2B 0x4E 0x72


linkColor =
    blue



-- VIEW


type alias Document =
    { title : String
    , body : Element Msg
    }


view : Model -> Browser.Document Msg
view model =
    let
        { title, body } =
            viewContent model
    in
    { title = title
    , body =
        [ Element.layout [] <|
            Element.column
                [ Element.width Element.fill ]
                [ viewHeader
                , body
                , viewFooter
                ]
        ]
    }


viewHeader : Element Msg
viewHeader =
    Element.row
        [ Element.height (Element.px 56)
        , Element.width Element.fill
        , Element.centerY
        , Element.spacing 16
        , Element.padding 8
        , Background.color blue
        , Font.color white
        ]
        [ Element.link [] { url = "/home", label = Element.el [] (Element.text "DSpace") }
        , Element.link [] { url = "/home", label = Element.el [] (Element.text "home") }
        ]


viewFooter : Element Msg
viewFooter =
    Element.row
        [ Element.height (Element.px 72)
        , Element.width Element.fill
        , Element.centerY
        , Element.spacing 2
        , Element.padding 10
        , Background.color grey
        ]
        [ Element.link [ Element.centerX, Font.color linkColor ]
            { url = "http://www.dspace.org/"
            , label = Element.el [] (Element.text "DSpace software")
            }
        , Element.el [ Element.centerX ] (Element.text "copyright © 2002-2018")
        , Element.link [ Element.centerX, Font.color linkColor ]
            { url = "http://www.duraspace.org/"
            , label = Element.el [] (Element.text "DuraSpace")
            }
        ]


viewContent : Model -> Document
viewContent model =
    case model.currentPage of
        Home ->
            viewHomePage model.homePage model.repository

        CommunityPage id ->
            viewCommunityPage model.repository id

        CollectionPage id ->
            viewCollectionPage model.repository id


withTitle : String -> Element Msg -> Document
withTitle title element =
    { title = title
    , body = element
    }


viewHomePage : SearchData -> Repository -> Document
viewHomePage homePage repository =
    case homePage of
        RemoteData.NotAsked ->
            Element.el [] (Element.text "initializing")
                |> withTitle "DSpace Home"

        RemoteData.Loading ->
            Element.el [] (Element.text "fetching remote data")
                |> withTitle "DSpace Home"

        RemoteData.Failure e ->
            viewError e "community list"
                |> withTitle "DSpace Home"

        RemoteData.Success data ->
            Element.column
                []
                (List.concat
                    [ [ Element.el [ Region.heading 1 ] (Element.text "DSpace repository") ]
                    , viewPage data.pagination
                    , viewSearch data.search
                    , viewCommunities repository
                    ]
                )
                |> withTitle "DSpace Home"


viewCommunityPage : Repository -> CommunityID -> Document
viewCommunityPage repository id =
    case Repository.getDSpaceObject repository id of
        RemoteData.NotAsked ->
            Element.el [] (Element.text "not asked")
                |> withTitle "DSpace Community"

        RemoteData.Loading ->
            Element.el [] (Element.text "loading")
                |> withTitle "DSpace Community"

        RemoteData.Failure e ->
            viewError e "community"
                |> withTitle "DSpace Community"

        RemoteData.Success dso ->
            viewCommunityPageLoaded repository dso
                |> withTitle (DSpace.getName dso)


viewCollectionPage : Repository -> CollectionID -> Document
viewCollectionPage repository id =
    case Repository.getDSpaceObject repository id of
        RemoteData.NotAsked ->
            Element.el [] (Element.text "not asked")
                |> withTitle "DSpace Collection"

        RemoteData.Loading ->
            Element.el [] (Element.text "loading")
                |> withTitle "DSpace Collection"

        RemoteData.Failure e ->
            viewError e "collection"
                |> withTitle "DSpace Collection"

        RemoteData.Success dso ->
            viewCollectionPageLoaded repository dso
                |> withTitle (DSpace.getName dso)


viewError : Http.Error -> String -> Element Msg
viewError error subject =
    case error of
        Http.BadStatus response ->
            if response.status.code == 404 then
                Element.el [] (Element.text <| subject ++ " not found")

            else
                Element.el [] (Element.text "error")

        _ ->
            Element.el [] (Element.text "error")


viewSearch : Maybe String -> List (Element Msg)
viewSearch maybe =
    case maybe of
        Nothing ->
            []

        Just location ->
            [ Element.link [ Font.color linkColor ] { url = "/search", label = Element.text "search" } ]


viewPage : Pagination -> List (Element Msg)
viewPage pagination =
    [ Element.el [] (Element.text <| String.fromInt pagination.size) ]


viewCommunities : Repository -> List (Element Msg)
viewCommunities repository =
    Repository.getCommunities repository
        |> List.map viewCommunity


viewCommunity : Community -> Element Msg
viewCommunity community =
    Element.column
        []
        [ Element.el
            [ Region.heading 1 ]
            (Element.link [ Font.color linkColor ]
                { url = toLocation (CommunityDso community)
                , label =
                    Element.text <| "Community: " ++ community.name
                }
            )
        , Element.text <| "handle: " ++ DSpace.handleToString community.handle
        ]


viewMetadatum : MetadataValue -> Element Msg
viewMetadatum metadatum =
    Element.text <| Metadata.getKeyString metadatum ++ ": " ++ metadatum.value


viewLoadingDso : List (WebData DSpaceObject) -> Element Msg
viewLoadingDso children =
    let
        count =
            children
                |> List.filter RemoteData.isLoading
                |> List.length
    in
    if count == 0 then
        emptyElement

    else
        Element.text <| "loading " ++ String.fromInt count ++ " dspace objects"


emptyElement : Element msg
emptyElement =
    Element.text ""


linkToCollection : Collection -> Element Msg
linkToCollection collection =
    Element.link [ Font.color linkColor ]
        { url = toLocation (CollectionDso collection)
        , label =
            Element.text collection.name
        }


viewCommunityPageLoaded : Repository -> DSpaceObject -> Element Msg
viewCommunityPageLoaded repository dso =
    case dso of
        CommunityDso community ->
            let
                children : List (WebData DSpaceObject)
                children =
                    Repository.getChildren repository dso
            in
            Element.column
                []
                [ Element.el [ Region.heading 1 ] (Element.text community.name)
                , children
                    |> List.filterMap RemoteData.toMaybe
                    |> DSpace.filterCollections
                    |> List.map linkToCollection
                    |> Element.column []
                , viewLoadingDso children
                , Element.column [] (List.map viewMetadatum community.metadata)
                ]

        _ ->
            Element.el [] (Element.text "not a community")


viewCollectionPageLoaded : Repository -> DSpaceObject -> Element Msg
viewCollectionPageLoaded repository dso =
    case dso of
        CollectionDso collection ->
            Element.column
                []
                [ Element.el [ Region.heading 1 ] (Element.text collection.name)
                , Element.column [] (List.map viewMetadatum collection.metadata)
                ]

        _ ->
            Element.el [] (Element.text "not a collection")
